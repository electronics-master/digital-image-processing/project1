
"""
This module generates a series of frame with syntethic
transformations
"""

from cv2 import warpAffine
import numpy as np
from scipy import signal


SAWTOOTH_WIDTH = 0.5  # This value gives a triangular waveform


def generate_synthetic_transform(img,
                                 x_scaling, y_scaling,
                                 x_displacement, y_displacement,
                                 rotation, num_frames):
    """Stabilizes a video stream

    Parameters
    ----------

    img : Souce image to transform

    x_scaling : Tuple with magnitude and frequency for scaling,
    magnitude is in pixels, period in frames.
    y_scaling : Tuple with magnitude and frequency for scaling,
    magnitude is in pixels, period in frames.

    x_displacement : Tuple with magnitude and frequency for
    displacement. Magnitude is as a percentage, period in frames.

    y_displacement : Tuple with magnitude and frequency for
    displacement. Magnitude is as a percentage, period in frames.

    rotation : Tuple with magnitude and frequency for rotation,
    magnitude is degrees, period in frames.

    num_frames : Number of frames to generate

    Returns
    -------

    List of transformed frames and corresponding transformations
    """
    assert num_frames > 1
    assert len(x_scaling) == 2
    assert len(y_scaling) == 2
    assert len(x_displacement) == 2
    assert len(y_displacement) == 2
    assert len(rotation) == 2

    if x_scaling[1] == 0:
        x_scaling = (x_scaling[0], np.Infinity)
    if y_scaling[1] == 0:
        y_scaling = (y_scaling[0], np.Infinity)
    if x_displacement[1] == 0:
        x_displacement = (x_displacement[0], np.Infinity)
    if y_displacement[1] == 0:
        y_displacement = (y_displacement[0], np.Infinity)
    if rotation[1] == 0:
        rotation = (rotation[0], np.Infinity)

    # Generate the values
    x = np.linspace(0, num_frames - 1, num_frames)
    sx_vals = 1 + x_scaling[0] / 100.0 * signal.sawtooth(
        2 * np.pi / x_scaling[1] * x + np.pi / 2, SAWTOOTH_WIDTH)
    sy_vals = 1 + y_scaling[0] / 100.0 * signal.sawtooth(
        2 * np.pi / y_scaling[1] * x + np.pi / 2, SAWTOOTH_WIDTH)
    dx_vals = x_displacement[0] * signal.sawtooth(
        2 * np.pi / x_displacement[1] * x + np.pi / 2, SAWTOOTH_WIDTH)
    dy_vals = y_displacement[0] * signal.sawtooth(
        2 * np.pi / y_displacement[1] * x + np.pi / 2, SAWTOOTH_WIDTH)
    rot_vals = rotation[0] * np.pi / 180.0 * signal.sawtooth(
        2 * np.pi / rotation[1] * x + np.pi / 2, SAWTOOTH_WIDTH)

    generated_frames = []
    transformations = []
    centering_mat = np.array([[1, 0, -0.5 * img.shape[1]],
                              [0, 1, -0.5 * img.shape[0]],
                              [0, 0, 1]])
    decentering_mat = np.array([[1, 0, 0.5 * img.shape[1]],
                                [0, 1, 0.5 * img.shape[0]],
                                [0, 0, 1]])
    for sx, sy, dx, dy, theta in zip(
            sx_vals, sy_vals, dx_vals, dy_vals, rot_vals):
        scale_mat = np.array([[sx, 0, 0],
                              [0, sy, 0],
                              [0, 0, 1]])
        rot_mat = np.array([[np.cos(theta), -np.sin(theta), 0],
                            [np.sin(theta), np.cos(theta), 0],
                            [0, 0, 1]])
        move_mat = np.array([[1, 0, dx],
                             [0, 1, dy],
                             [0, 0, 1]])

        transformation = np.matmul(
            move_mat, np.matmul(
                decentering_mat, np.matmul(
                    scale_mat, np.matmul(
                        rot_mat, centering_mat))))
        M = transformation[0:2, 0:3]
        warped_img = warpAffine(img, M, (img.shape[1], img.shape[0]))

        generated_frames.append(warped_img)
        transformations.append(
            (transformation[0:2, 0:2], transformation[0:2, 2]))

    return generated_frames, transformations
