#! /usr/bin/env python3

import cv2
import numpy as np
from scipy import ndimage, linalg

from stabilize import videostabilize
from synthetic_transform import generate_synthetic_transform


import argparse
import mimetypes


def video_or_image(filename):
    mimetypes.init()

    mimestart = mimetypes.guess_type(filename)[0]

    if mimestart is not None:
        mimestart = mimestart.split('/')[0]

        if mimestart == "video" or mimestart == "image":
            return mimestart
        else:
            return None


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Stabilize a video sequence or "
        "a syntetic video based on a single image")
    parser.add_argument("-i", "--input_path",
                        help="Path to a single image or a video",
                        type=str,
                        required=True)

    parser.add_argument("-l", "--levels",
                        help="Number of levels for the pyramid",
                        type=int,
                        default=5)
    parser.add_argument(
        "-n",
        "--num_frames",
        help="Number of frames to process",
        type=int,
        default=20)

    parser.add_argument(
        "-sx",
        "--x_scaling",
        help="Magnitude and frequency for horizontal scaling, magnitude is in pixels",
        nargs=2,
        type=int,
        metavar=(
            "SX_MAG",
            "SX_FREQ"),
        default=(
            0,
            0))
    parser.add_argument(
        "-sy",
        "--y_scaling",
        help="Magnitude and frequency for vertical scaling, magnitude is in pixels",
        nargs=2,
        type=int,
        metavar=(
            "SY_MAG",
            "SY_FREQ"),
        default=(
            0,
            0))
    parser.add_argument(
        "-tx",
        "--x_displacement",
        help="Magnitude and frequency for horizontal shift, magnitude is in pixels",
        nargs=2,
        type=int,
        metavar=(
            "TX_MAG",
            "TX_FREQ"),
        default=(
            0,
            0))
    parser.add_argument(
        "-ty",
        "--y_displacement",
        help="Magnitude and frequency for vertical shift, magnitude is in pixels",
        nargs=2,
        type=int,
        metavar=(
            "TY_MAG",
            "TY_FREQ"),
        default=(
            0,
            0))
    parser.add_argument(
        "-theta",
        "--rotation",
        help="Magnitude and frequency for rotation, magnitude is in degrees",
        nargs=2,
        type=int,
        metavar=(
            "THETA_MAG",
            "THETA_FREQ"),
        default=(
            0,
            0))
    parser.add_argument(
        "-test",
        type=bool,
        default=False)

    return parser.parse_args()


def process_image(filename,
                  x_scaling, y_scaling,
                  x_displacement, y_displacement,
                  rotation,
                  num_frames,
                  levels):
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)

    print("Generating synthetic transformation")

    frames, transformations = generate_synthetic_transform(
        img, x_scaling, y_scaling, x_displacement, y_displacement, rotation, num_frames)
    print("Finished generating synthetic transformation")

    roi = cv2.selectROI(img)
    roi = np.array([[roi[1], roi[0]], [roi[1] + roi[3], roi[0] + roi[2]]])

    stabilized_frames, motion, cum_motion = videostabilize(frames, roi, levels)

    print("Finished stabilizing video sequence")

    # Error calculation
    MSE = []
    for trans, move in zip(transformations, cum_motion):
        MSE.append(
            np.square(
                trans[0] -
                move[0]).mean(
                axis=None) +
            np.square(
                trans[1] -
                move[1]).mean(
                    axis=None))

    print("MSE: ", sum(MSE))

    # Display the resulting frame
    for frame, stabilized_frame, mat in zip(frames, stabilized_frames, motion):
        cv2.imshow('original', frame)
        cv2.imshow('stabilized', stabilized_frame)
        c = cv2.waitKey()
        if c == 113: # q
            break

    # When everything done, release the capture
    cv2.destroyAllWindows()


def process_video(filename):
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    # TODO

    print("Stabilizing video sequence")

    stabilized_frames, motion = videostabilize(frames, roi, L)

    print("Finished stabilizing video sequence")

    # Display the resulting frame
    for frame, stabilized_frame, mat in zip(frames, stabilized_frames, motion):
        cv2.imshow('original', frame)
        cv2.imshow('stabilized', stabilized_frame)
        cv2.waitKey()

    # When everything done, release the capture
    cv2.destroyAllWindows()

def process_test(filename):
    test_params = [
        # X_Scaling,
        [(0, 0), (0, 0), (10, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (20, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (30, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (40, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (50, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (60, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (70, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (80, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (90, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (100, 10), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (10, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (20, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (30, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (40, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (50, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (60, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (70, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (80, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (90, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (100, 20), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (10, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (20, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (30, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (40, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (50, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (60, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (70, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (80, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (90, 30), (0, 0), (0, 0), 20, 4],
        [(0, 0), (0, 0), (100, 30), (0, 0), (0, 0), 20, 4],
        # X_Translation,
        [(10, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(20, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(30, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(40, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(50, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(60, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(70, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(80, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(90, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(100, 10), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(10, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(20, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(30, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(40, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(50, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(60, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(70, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(80, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(90, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(100, 30), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        # Rotation,
        [(0, 0), (0, 0), (0, 0), (0, 0), (10, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (20, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (30, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (40, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (50, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (60, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (70, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (80, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (90, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (100, 10), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (10, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (20, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (30, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (40, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (50, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (60, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (70, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (80, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (90, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (100, 20), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (10, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (20, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (30, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (40, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (50, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (60, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (70, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (80, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (90, 30), 20, 4],
        [(0, 0), (0, 0), (0, 0), (0, 0), (100, 30), 20, 4],
        # Levels
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 1],
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 2],
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 4],
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 6],
        [(10, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(20, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(30, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(40, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(50, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(60, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(70, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(80, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(90, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
        [(100, 20), (0, 0), (0, 0), (0, 0), (0, 0), 20, 8],
    ]
    
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)

    print("Generating synthetic transformation")
    
    roi = np.array([[20, 250], [300, 450]])

    for params in test_params:

        frames, transformations = generate_synthetic_transform(
            img, params[0], params[1], params[2], params[3], params[4], params[5])
        print("Finished generating synthetic transformation")


        stabilized_frames, motion, cum_motion = videostabilize(frames, roi, params[6])

        print("Finished stabilizing video sequence")

        # Error calculation
        MSE = []
        for real_trans, calc_trans in zip(transformations, cum_motion):
            MSE.append(
                np.square(
                    real_trans[0] -
                    calc_trans[0]).mean(
                        axis=None) +
                np.square(
                    real_trans[1] -
                    calc_trans[1]).mean(
                        axis=None))

        print("params: ", params)
        print("MSE: ", sum(MSE))

if __name__ == "__main__":
    args = parse_arguments()
    print(args)

    if args.test:
        process_test(args.input_path)
        exit(0)

    img_type = video_or_image(args.input_path)

    if img_type == "image":
        process_image(args.input_path,
                      args.x_scaling, args.y_scaling,
                      args.x_displacement, args.y_displacement,
                      args.rotation,
                      args.num_frames,
                      args.levels)
