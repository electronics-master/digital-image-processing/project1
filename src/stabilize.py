
"""
This module provides utilities for video stabilization based on
the paper Video Enhacement and Stabilization by Farid and Woodward.
"""

from cv2 import warpAffine
import numpy as np
from scipy import signal


def videostabilize(frames, roi, levels):
    """
    Stabilizes a video stream

    Parameters
    ----------

    frames : List of frames to be stabilized
    roi : 2x2 numpy array with the corners of the region of interest to stabilize.
    levels : Number of pyramid levels

    Returns
    -------

    List of stabilized frames, and a list of tuples with the affine
    matrix and the translation vector
    """
    assert len(frames) >= 2
    assert roi.shape == (2, 2)
    assert levels >= 0

    cum_affine_mat = np.array([[1, 0], [0, 1]])
    cum_trans_vec = np.array([0, 0])

    # Convert ROI to a mask
    x0 = np.int(roi[0][0])
    y0 = np.int(roi[0][1])
    x1 = np.int(roi[1][0]) + 1
    y1 = np.int(roi[1][1]) + 1

    roi = np.zeros(frames[0].shape)
    roi[x0:x1, y0:y1] = 1.0

    motion = []
    cum_motion = [(cum_affine_mat, cum_trans_vec)]
    stabilized_images = []
    # Estimate pairwise motion
    for old_frame, frame in zip(frames[:-1], frames[1:]):
        warped_frame, affine_mat, trans_vec = stabilize(
            frame, old_frame, roi, levels)
        motion.append((affine_mat, trans_vec))

        cum_affine_mat, cum_trans_vec = accumulate_warp(
            cum_affine_mat, cum_trans_vec, affine_mat, trans_vec)
        cum_motion.append((cum_affine_mat, cum_trans_vec))
        roi = warp(roi, affine_mat, trans_vec)

    # Append last frame
    stabilized_images = [frames[-1]]

    # Stabilize to the last frame
    cum_affine_mat = np.array([[1, 0], [0, 1]])
    cum_trans_vec = np.array([0, 0])
    for frame, move in zip(reversed(frames[:-1]), reversed(motion[:-1])):
        cum_affine_mat, cum_trans_vec = accumulate_warp(
            cum_affine_mat, cum_trans_vec, move[0], move[1])
        stabilized_images.insert(0, warp(frame,
                                         cum_affine_mat, cum_trans_vec))

    return stabilized_images, motion, cum_motion


def stabilize(last_frame, this_frame, roi, levels):
    """
    Returns the stabilized frame and the affine and the translation matrix.

    Parameters
    ----------

    last_frame : Nth-1 frame to perform transform over
    this_frame : Nth frame to perform transform over
    roi : 2x2 numpy array with the corners of the region of interest to stabilize.
    levels : Number of pyramid levels

    Returns
    -------

    Affine matrix and translation vector for the corresponding ROI
    """
    assert levels >= 0
    # Assert number of levels, higher number than this appears to be unable to
    # be stabilized
    assert ((2 ** levels) <=
            last_frame.shape[0]) or ((2 ** levels) <= last_frame.shape[1])
    assert last_frame.shape == this_frame.shape

    cum_affine_mat = np.array([[1, 0], [0, 1]])
    cum_trans_vec = np.array([0, 0])

    orig_frame = this_frame

    # This for loop creates the gaussian pyramid by calling the
    # downsampling function which blurs and reduces the image
    # resolution.
    for k in reversed(range(0, levels)):
        downsampled_last_frame = downsample(last_frame, k)
        downsampled_this_frame = downsample(this_frame, k)
        downsampled_roi = downsample(roi, k)

        x_deriv, y_deriv, t_deriv = spacetimederiv(
            downsampled_last_frame, downsampled_this_frame)
        affine_mat, trans_vec = computemotion(
            x_deriv, y_deriv, t_deriv, downsampled_roi)
        trans_vec = (2 ** k) * trans_vec

        # Add previous image warps
        cum_affine_mat, cum_trans_vec = accumulate_warp(
            cum_affine_mat, cum_trans_vec, affine_mat, trans_vec)

        this_frame = warp(orig_frame, cum_affine_mat, cum_trans_vec)

    return this_frame, cum_affine_mat, cum_trans_vec


def computemotion(fx, fy, ft, roi):
    """
    Computes motion for a pair of frames.

    This is done according to space-timer derivates in a region of interest

    Parameters
    ----------

    fx : X axis derivate
    fy : Y axis derivate
    ft : Time derivate

    Returns
    -------

    Affine matrix and translation vector for the corresponding ROI
    """
    assert fx.shape == fy.shape == ft.shape

    # Get Roi
    y, x = np.where(roi > 0)
    fx = fx[y, x]
    fy = fy[y, x]
    ft = ft[y, x]

    xfx = np.multiply(x, fx)
    xfy = np.multiply(x, fy)
    yfx = np.multiply(y, fx)
    yfy = np.multiply(y, fy)

    c = [xfx, yfx, xfy, yfy, fx, fy]

    M = np.zeros([6, 6])
    for i, vec_x in enumerate(c):
        for j, vec_y in enumerate(c):
            M[i][j] = np.sum(np.multiply(vec_x, vec_y))

    k = ft + xfx + yfy
    b = np.zeros(6)
    for i, vec in enumerate(c):
        b[i] = np.sum(np.multiply(vec, k))

    v = np.matmul(np.linalg.pinv(M), np.transpose(b))
    affine_mat = np.array([[v[0], v[1]], [v[2], v[3]]])
    trans_vec = np.array([v[4], v[5]])

    return affine_mat, trans_vec


def spacetimederiv(last_frame, this_frame):
    """
    Performs derivates in space and time.

    Parameters
    ----------
    last_frame : Nth-1 frame as a numpy image
    this_frame : Nth frame as a numpy image

    Returns
    -------
    Image derivates for x and y axis, and time derivate
    """
    deriv = np.array([0.5, -0.5])
    pre = np.array([0.5, 0.5])

    # Ensure that they are correct 2D matrices
    deriv = deriv[None, :]
    pre = pre[None, :]

    x_deriv = signal.convolve2d(
        signal.convolve2d(
            0.5 *
            this_frame +
            0.5 *
            last_frame,
            deriv,
            mode="same"),
        np.transpose(pre),
        mode="same")
    y_deriv = signal.convolve2d(
        signal.convolve2d(
            0.5 *
            this_frame +
            0.5 *
            last_frame,
            pre,
            mode="same"),
        np.transpose(deriv),
        mode="same")
    t_deriv = signal.convolve2d(
        signal.convolve2d(
            0.5 *
            this_frame -
            0.5 *
            last_frame,
            pre,
            mode="same"),
        np.transpose(pre),
        mode="same")

    return x_deriv, y_deriv, t_deriv


def warp(img, affine_mat, trans_vec):
    """
    Warps an image according to an affine matrix and a translation vector.

    Parameters
    ----------
    img : numpy image
    affine_mat : 2x2 Affine matrix
    trans_vec : 2x1 Translation vector

    Returns
    -------
    Warped image
    """
    assert affine_mat.shape == (2, 2)
    assert len(trans_vec) == 2

    M = np.zeros([2, 3])

    M[0][0] = affine_mat[0][0]
    M[0][1] = affine_mat[0][1]
    M[1][0] = affine_mat[1][0]
    M[1][1] = affine_mat[1][1]
    M[0][2] = trans_vec[0]
    M[1][2] = trans_vec[1]

    return warpAffine(img, M, (img.shape[1], img.shape[0]))


def accumulate_warp(cum_affine_mat, cum_trans_vec, affine_mat, trans_vec):
    """
    Accumulates the affine matrix and translation vector.

    Parameters
    ----------
    cum_affine_mat : Cummulative affine matrix
    cum_trans_vec : Cummulative translation vector
    affine_mat : New affine matrix to be accumulated into the cummulative affine matrix
    trans_vec : New translation vector to be accumulated into the cummulative translation vector

    Returns
    -------
    New cummulative affine matrix and translation vector
    """
    assert affine_mat.shape == (2, 2)
    assert len(trans_vec) == 2
    assert cum_affine_mat.shape == (2, 2)
    assert len(cum_trans_vec) == 2

    new_cum_affine_mat = np.matmul(affine_mat, cum_affine_mat)
    new_cum_trans_vec = np.matmul(affine_mat, cum_trans_vec) + trans_vec

    return new_cum_affine_mat, new_cum_trans_vec


def downsample(image, levels):
    """
    Performs a downsampling on an image.
    Downsampling is done by blurring and reducing the resolution of the image

    Parameters
    ----------
    image : Image to be downsampled
    levels : Levels to downsample the image

    Returns
    -------
    Downsampled image
    """
    assert levels >= 0

    blur = np.array([[0.25, 0.5, 0.25]])

    for k in range(levels):
        image = signal.convolve2d(
            signal.convolve2d(image, blur, mode="same"),
            np.transpose(blur), mode="same")
        image = image[::2, ::2]
    return image
